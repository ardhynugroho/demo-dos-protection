# DNS

## Create example.lab zone

1. DNS > Zones > ZoneRunner > Zone List
2. Click button: Create
    - View Name: external
    - Zone Name: example.lab
    - SOA Record > TTL: 300
    - SOA Record > Master Server: ns
    - NS Record > TTL : 300
    - NS Record > Nameserver: ns
    - A Record: 10.1.2.3

3. Also notify DNS-Express on record update.
    Add below config in Configuration > Options
    ```
    also-notify {
        ::1 port 5353;
    };
    ```
4. Finished

## Add new record

1. DNS > Zones > ZoneRunner > Zone List > example.lab
2. Click button: Add Resource Record
3. Input parameters:
    - Name: www.example.lab
    - TTL: 300
    - Type: A
    - IP Address: 10.11.22.33
    - Create Reverse Record: clear-check
4. Click button: Repeat
5. Input parameters:
    - Name: api.example.lab
    - TTL: 300
    - Type: A
    - IP Address: 10.11.22.34
    - Create Reverse Record: clear-check
5. Click button: Finished

## Test DNS Authoritative

```
dig @10.1.10.56 www.example.lab
```

## Test DNS Cache

```
dnsperf -s 10.1.10.53 -d example.com -c 30 -T 30 -l 30 -t 30 -q 30000
```

Check backend DNS CPU utilization

```
docker stats
```

It should be in low utilization because DNS responded from F5 DNS cache
