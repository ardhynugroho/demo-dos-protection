@  3600  IN  SOA  ns1.example.com.  root.example.com.  (
   2019041900
   3600
   600
   604800
   600 )
   3600  IN  NS  ns1.example.com
   3600  IN  NS  ns2.example.com

ns1      IN  A  10.0.0.53
         IN  AAAA  2001:db8:42:1::53
ns2      IN  A  10.0.1.53
         IN  AAAA  2001:db8:42:2::53
www      IN  A  10.0.0.1
         IN  AAAA  2001:db8:42:1::1
api      IN  A  10.0.0.12
         IN  AAAA  2001:db8:42:1::12