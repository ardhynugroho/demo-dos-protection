# DNS DoS

```mermaid
dnsperf --->|10.1.10.53| vs_dns ---> backend_dns
dnsperf --->|10.1.50.54| vs_dns_dos ---> backend_dns
```

## Backend DNS

```
docker compose up -d
```

## Virtual Server

```
tmsh load sys config merge from-terminal
```

## Attack

Unprotected

```
dnsperf -s 10.1.10.53 -d example.com -c 30 -T 30 -l 30 -t 30 -q 30000
```

Protected

```
dnsperf -s 10.1.10.54 -d example.com -c 30 -T 30 -l 30 -t 30 -q 30000
```